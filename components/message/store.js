const db = require('mongoose');
const Model = require('./model');

db.Promise = global.Promise;
const uri = 'mongodb://root:Secret123@cluster0-shard-00-02-a9wrf.mongodb.net,cluster0-shard-00-01-a9wrf.mongodb.net/telegrom?ssl=true&replicaSet=Main-shard-0&authSource=admin&retryWrites=true';

db.Promise = global.Promise;

db.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('[db] Conectada con éxito'))
  .catch(err => console.error('[db]', err));

function addMessage(message){
    // list.push(message);
    const myMessage = new Model(message);
    myMessage.save();
}

async function getMessaged(filterUser){
    let filter = {};
    if(filterUser!==null){
        filter = { user: filterUser};
    }
    const messages = await Model.find(filter);
    return messages;
}

async function updateText(id, message){
    const foundMessage = await Model.findOne({
        _id: id
    });
    foundMessage.message = message;
    const newMessage = await foundMessage.save();
    return newMessage;
}

module.exports = {
    add: addMessage,
    list: getMessaged,
    updateText: updateText,
    //delete
} 